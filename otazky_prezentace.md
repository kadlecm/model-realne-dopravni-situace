# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** |4-5h |
| jak se mi to podařilo rozplánovat |plánování?|
| návrh designu |[Návrh](https://gitlab.spseplzen.cz/kadlecm/model-realne-dopravni-situace/-/blob/main/dokumentace/design/navrh.png)|
| proč jsem zvolil tento design |Abych splnil zadání|
| zapojení |[Link](https://gitlab.spseplzen.cz/kadlecm/model-realne-dopravni-situace/-/blob/main/dokumentace/design/Tinkercad.png)|
| z jakých součástí se zapojení skládá |10 led,kabely,tlačítko,fotoresistor,arduino,rezistory|
| realizace |[ *URL obrázku hotového produktu* ](https://gitlab.spseplzen.cz/kadlecm/model-realne-dopravni-situace/-/blob/main/dokumentace/fotky/PXL_20240404_181109361.jpg)|
| jaký materiál jsem použil a proč |Krabici od bot protože byla po ruce|
| co se mi povedlo |zapojení|
| co se mi nepovedlo/příště bych udělal/a jinak |vzhled|
| zhodnocení celé tvorby |Nic moc, ale na 1 den celkem ujde|
